// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package screengen

import (
	"bytes"
	"image"
	"testing"
)

func TestRotate90(t *testing.T) {
	t.Parallel()
	m := image.NewRGBA(image.Rect(0, 0, 3, 4))
	for i := range m.Pix {
		m.Pix[i] = uint8(i)
	}
	m = rotate90(m)
	size := m.Rect.Size()
	if size.X != 4 || size.Y != 3 {
		t.Fatalf("want size %dx%d, got %dx%d", 4, 3, size.X, size.Y)
	}
	want := []uint8{36, 37, 38, 39, 24, 25, 26, 27, 12, 13, 14, 15, 0, 1, 2, 3, 40, 41, 42, 43, 28, 29, 30, 31, 16, 17, 18, 19, 4, 5, 6, 7, 44, 45, 46, 47, 32, 33, 34, 35, 20, 21, 22, 23, 8, 9, 10, 11}
	if !bytes.Equal(m.Pix, want) {
		t.Errorf("want %v, got %v", want, m.Pix)
	}
}

func TestRotate180(t *testing.T) {
	t.Parallel()
	m := image.NewRGBA(image.Rect(0, 0, 3, 4))
	for i := range m.Pix {
		m.Pix[i] = uint8(i)
	}
	m = rotate180(m)
	size := m.Rect.Size()
	if size.X != 3 || size.Y != 4 {
		t.Fatalf("want size %dx%d, got %dx%d", 3, 4, size.X, size.Y)
	}
	want := []uint8{44, 45, 46, 47, 40, 41, 42, 43, 36, 37, 38, 39, 32, 33, 34, 35, 28, 29, 30, 31, 24, 25, 26, 27, 20, 21, 22, 23, 16, 17, 18, 19, 12, 13, 14, 15, 8, 9, 10, 11, 4, 5, 6, 7, 0, 1, 2, 3}
	if !bytes.Equal(m.Pix, want) {
		t.Errorf("want %v, got %v", want, m.Pix)
	}
}

func TestRotate180Odd(t *testing.T) {
	t.Parallel()
	m := image.NewRGBA(image.Rect(0, 0, 4, 3))
	for i := range m.Pix {
		m.Pix[i] = uint8(i)
	}
	m = rotate180(m)
	size := m.Rect.Size()
	if size.X != 4 || size.Y != 3 {
		t.Fatalf("want size %dx%d, got %dx%d", 4, 3, size.X, size.Y)
	}
	want := []uint8{44, 45, 46, 47, 40, 41, 42, 43, 36, 37, 38, 39, 32, 33, 34, 35, 28, 29, 30, 31, 24, 25, 26, 27, 20, 21, 22, 23, 16, 17, 18, 19, 12, 13, 14, 15, 8, 9, 10, 11, 4, 5, 6, 7, 0, 1, 2, 3}
	if !bytes.Equal(m.Pix, want) {
		t.Errorf("want %v, got %v", want, m.Pix)
	}
}

func TestRotate270(t *testing.T) {
	t.Parallel()
	m := image.NewRGBA(image.Rect(0, 0, 3, 4))
	for i := range m.Pix {
		m.Pix[i] = uint8(i)
	}
	m = rotate270(m)
	size := m.Rect.Size()
	if size.X != 4 || size.Y != 3 {
		t.Fatalf("want size %dx%d, got %dx%d", 4, 3, size.X, size.Y)
	}
	want := []uint8{8, 9, 10, 11, 20, 21, 22, 23, 32, 33, 34, 35, 44, 45, 46, 47, 4, 5, 6, 7, 16, 17, 18, 19, 28, 29, 30, 31, 40, 41, 42, 43, 0, 1, 2, 3, 12, 13, 14, 15, 24, 25, 26, 27, 36, 37, 38, 39}
	if !bytes.Equal(m.Pix, want) {
		t.Errorf("want %v, got %v", want, m.Pix)
	}
}
